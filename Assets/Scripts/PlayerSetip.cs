﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetip : NetworkBehaviour {


	//list of compontents you want to disable 
	[SerializeField] //lets you see it ininspector 
	public Behaviour[] componentsToDisable;

	[SerializeField]
	private int remoteLayerName = 9; 
	//string remoteLayerName = "RemotePlayer";

	Camera sceneCamera;

	void Start()
	{

		//check if on network
		if (!isLocalPlayer) {
			DisableComponents ();
			AssignRemoteLayer ();			
		} else {
			sceneCamera = Camera.main; 

			//this is so if we dont find the camera we dont get any errors. 
			if (sceneCamera != null) {
				//disables the main camera for the scene for the local player. 
				Camera.main.gameObject.SetActive (false);
			}
		}



	}

	void AssignRemoteLayer()
	{
		gameObject.layer = remoteLayerName; //LayerMask.NameToLayer (remoteLayerName);
	}

	void DisableComponents()
	{
		//go through list of all the components you want to get rid of
		for (int i = 0; i < componentsToDisable.Length; i++) {
			//disable here 
			componentsToDisable [i].enabled = false; 
		}		
	}

	void OnDisable()
	{
		if (sceneCamera != null) {
			//reenable the camera if you are disconnecting from level 
			sceneCamera.gameObject.SetActive (true);
		}
	}


}
